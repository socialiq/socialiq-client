var passport = require('passport'),
	application = require('./lib/application'),
	strategy = require('./lib/strategy');

/**
  Initialize the Social IQ client
 **/
exports.init = function(opts) {
	if (!opts.app_secret) throw new Error('No application secret provided');
	if (!opts.app_id) throw new Error('No application ID provided');

	application.opts = opts;
	passport.serializeUser(function(user, done) {
		var ok = (user && user.user_id);
		if (!ok) return done('pass');
		done(null, JSON.stringify(user));
	});

	passport.deserializeUser(function(user, done) {
		var results = JSON.parse(user);
		if (results && results.user_id) {
			return done(null, JSON.parse(user));
		}
		return done('pass');
	});

	application.environment = require('./lib/environment')(opts.environment || 'PRODUCTION');
	passport.use(new strategy.SignedRequestTokenStrategy(application.opts));	
	passport.use(new strategy.SignedRequestStrategy(application.opts));	

}

/**
  Add an additional authorization strategy
 **/
exports.add = function(opts) {
	if (!opts.app_secret) throw new Error('No application secret provided');
	if (!opts.app_id) throw new Error('No application ID provided');

	passport.use(new strategy.SignedRequestTokenStrategy(opts));
	passport.use(new strategy.SignedRequestStrategy(opts));
}

/**
  Expose passport
 **/
exports.passport = passport;

/**
  Expose the connect middleware
 **/
exports.connect = require('./lib/connect');

exports.middleware = require('./lib/middleware');