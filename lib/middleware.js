var passport = require('passport');

exports.authenticated = passport.authenticate('socialiq-token');

exports.authenticatedStrategy = function(strategy) { return passport.authenticate(strategy); }