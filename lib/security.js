var crypto = require('crypto');

/**
  Parses an encrypted object
 **/
exports.parseRequest = function(data, secret) {
	if (!data || !secret) return null;

	var decipher = crypto.createDecipher('aes192', secret),
		results = decipher.update(data, 'base64', 'utf8');

	results += decipher.final('utf8');
	return JSON.parse(results);
}