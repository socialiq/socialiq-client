var passport = require('passport'),
    util = require('util'),
    request = require('request'),
    security = require('./security'),
    application = require('./application');

function SignedRequestTokenStrategy(opts) {
  this.opts = opts;
  passport.Strategy.call(this);
  this.name = (opts.name || 'socialiq') + '-token';
}
util.inherits(SignedRequestTokenStrategy, passport.Strategy);

/**
 * Authenticate request based on a Social.iQ redeemable request token
 *
 * @param {Object} req
 */
SignedRequestTokenStrategy.prototype.authenticate = function(req, options) {

    var signedRequest = req.body.qsr || req.query.qsr,
        self = this,
        instanceOpts = req.signedRequestOptions || self.opts;

    if (!signedRequest) return self.fail(new Error('Invalid signed request'));

    var parsedRequest = security.parseRequest(signedRequest, instanceOpts.app_secret);
    if (!parsedRequest) return self.fail(new Error('Invalid signed request'));
    if (!parsedRequest.token) return self.fail(new Error('Invalid token'));

    var url = application.environment.urls.redeem;
    request({
        method: 'POST',
        url: url,
        json: { token: parsedRequest.token, app_key: instanceOpts.app_id }
    }, function(err, response, body) {
        if (err || !body) return self.fail(new Error('Could not validate token'));

        if (response.statusCode == 200) {
            return self.success(body);    
        } else {
            return self.fail(new Error('Could not validate token'));
        }
        
    });
}

/**
 * Authenticates a request based on signed request package
 */
function SignedRequestStrategy(opts) {
    this.opts = opts;
    passport.Strategy.call(this);
    this.name = (opts.name || 'socialiq') + '-signed';
}
util.inherits(SignedRequestStrategy, passport.Strategy);

/**
 * Authenticate request based on a signed request
 *
 * @param {Object} req
 */
SignedRequestStrategy.prototype.authenticate = function(req, options) {

    var signedRequest = req.body.ssr || req.query.ssr || req.headers.ssr,
        self = this,
        instanceOpts = req.signedRequestOptions || self.opts;

    if (!signedRequest && typeof req.header == 'function') signedRequest = req.header('ssr');
    if (!signedRequest) return self.fail(new Error('Invalid signed request'));

    var parsedRequest = security.parseRequest(signedRequest, instanceOpts.app_secret);
    if (!parsedRequest) return self.fail(new Error('Invalid signed request'));
    return self.success(parsedRequest);
}


/**
 * Expose `Strategy`.
 */ 
module.exports = {
    SignedRequestTokenStrategy: SignedRequestTokenStrategy,
    SignedRequestStrategy: SignedRequestStrategy
};