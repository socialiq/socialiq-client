var serverUrl = {
	LOCAL: 'http://api.socialiqdev.io',
	TEST: 'https://api-test.socialiqapp.com',
	PRODUCTION: 'https://api.socialiqapp.com'
};

module.exports = function(environment) {
	
	var baseUrl = serverUrl[environment];
	return {
		urls: {
			redeem: baseUrl + '/auth/redeem'
		}
	};
}